import 'package:flutter/material.dart';

class DaftarProdukScreen extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Daftar Produk"),
        actions:[
          IconButton(
            onPressed: (){
              print("Tes");
              Navigator.pop(context);
            }, 
            icon: Icon(Icons.accessibility),
          )
        ]
      ),
      body: Container(
        child: Center(
          child: Column(
            children: buildRows(),
          ),
        )
      ),
    );
  }

  List<Widget> buildRows() {

    List<Widget> tempRow = [];
    for (var i = 0; i < 9; i++) {
      tempRow.add(
        Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text("Reza"),
                Text("Reza"),
                Text("Reza"),
              ],
        ));
    }
    return tempRow;
  }
}
