import 'package:flutter/material.dart';
import 'screen/about.dart';
import 'screen/daftarProduk.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Tutor'),
      routes: {
        '/about': (context){
          return AboutScreen();
        },
        '/daftarProduk': (context){
          return DaftarProdukScreen();
        },
      },
    );
  }
}



class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  int _currentIndexTab = 1;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        leading: Icon(Icons.access_time),
        actions: [
          IconButton(
            icon: Icon(Icons.access_time), 
            onPressed: (){
              print("haha");
            },
          ),
          Icon(Icons.accessible_forward),
        ]
        
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndexTab,
        items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.accessibility),
              title: Text("hehe"),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.accessibility),
              title: Text("hehe"),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.accessibility),
              title: Text("hehe"),
            ),
        ],
        onTap:(index){
          setState((){
            _currentIndexTab = index;
          });
        }
      ),
      body: Center(
        child:
          this._currentIndexTab == 0 ?
         Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            MaterialButton(
              child: Text("About page"),
              onPressed: () {
                Navigator.pushNamed(context, '/about');
              },
            ),
            MaterialButton(
              child: Text("Daftar Produk Page"),
              onPressed: () {
                Navigator.pushNamed(context, '/daftarProduk');
              },
            ),
            Text(
               "hahaha",
               style: TextStyle(
                 color: Colors.red,
               ),
            ),
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ): AboutScreen()
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      )
    );
  }
}
